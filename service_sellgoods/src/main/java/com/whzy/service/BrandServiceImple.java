package com.whzy.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.yz.core.dao.good.BrandDao;
import com.yz.core.pojo.good.Brand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BrandServiceImple implements BrandService{
    @Autowired
    private BrandDao brandDao;
    @Override
    public String findAll() {
        List<Brand> brandList = brandDao.selectByExample(null);
        System.out.println("brandList = " + brandList);
        return "123456";
    }
}
