package com.whzy.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.whzy.service.BrandService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/brand")
public class BrandController {
    @Reference
    private BrandService brandService;

    @RequestMapping("/findAll.do")
    public String findAll() {
        String all = brandService.findAll();
        return all;
    }
}
