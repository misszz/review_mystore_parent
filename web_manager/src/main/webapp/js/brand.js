new Vue({
    el: "#app",
    data: {
        brandList: [],
        brand: {}
    },
    methods: {
        findAllBrand: function () {
            var _this = this;
            //查询用户列表
            axios.get("/brand/findAll.do")
                .then(function (response) {
                    //取服务端响应的结果
                    _this.brandList = response.data;
                    console.log(response);
                })
                .catch(function (reason) {
                    console.log(reason);
                })
        },
    },
    created: function () {
        this.findAllBrand();
    }
});